const express = require("express");
const fileUpload = require('express-fileupload');
const mysql = require("mysql2");
  
const connection = mysql.createConnection({
	host: "localhost",
	user: "root",
	database: "test",
	password: "12345"
});

connection.connect(function(err){
	if (err) {
		return console.error("Ошибка: " + err.message);
    }
    else{
		console.log("Подключение к серверу MySQL успешно установлено");
    }
 });

const app = express();
app.use(fileUpload({}));

const urlencodedParser = express.urlencoded({extended: false}); 


app.get("/", function(req, res) {

	res.sendFile(__dirname + "/public/index.html");

});


app.get("/user/register", function(req, res) {

	res.sendFile(__dirname + "/public/register.html");

});
app.post("/user/register", urlencodedParser, function(req, res) {

	connection.execute(`INSERT INTO users 
						(first_name, email, password, date) 
		VALUES ('${req.body.name}', '${req.body.email}', '${req.body.passwd}', CURRENT_DATE());`,
		
		function(err, results, fields) {
		console.log(err);
		console.log(results); // собственно данные
	});

	connection.execute(`SELECT id FROM users
						WHERE first_name='${req.body.name}';`,
		
		function(err, results, fields) {
		console.log(err);
		console.log(results);
		if (!err)
			res.send(`Пользователь ${req.body.name} создан c id ${results[0].id}<br>
				<a href='/'>НАЗАД</a>`);
		 else 
			res.send("ЧТО-ТО ПОШЛО НЕ ТАК!");
	});


});

app.get("/user/login", function(req, res) {
	
	res.sendFile(__dirname + "/public/login.html");

});
app.post("/user/login", urlencodedParser, function(req, res) {

	connection.execute(`SELECT id FROM users 
						WHERE first_name = '${req.body.name}' 
						AND password = '${req.body.passwd}';`,
		
		function(err, results, fields) {
		console.log(err);
		if (results != 0)
			res.redirect(`/profile/${results[0].id}`);
		else
			res.send("ТАКОГО ПОЛЬЗОВАТЕЛЯ НЕТ, ЛИБО ВВЕДЕНЫ НЕВЕРНЫЕ ДАННЫЕ");
	});	

});


app.get("/profile/:id", function(req, res) {

	app.use(express.static("public"));
	
	const id = req.params.id;

	connection.execute(`SELECT * FROM users
						WHERE id = ${id};`,
		
		function(err, results, fields) {
		console.log(err);
	res.send(`
	<!DOCTYPE html>
	<html>
    <head>
    <title>${results[0].first_name}</title>
    </head>
    <body>

<img src="/pics/${results[0].photo}" width="100ex" height="100ex">
<form action="/upload/${id}" method="POST" enctype="multipart/form-data" >
	<input type="file" name="photo">
	<input type="submit">
</form>	

<form method="post">
    <p>
        <label>Имя:</label><br>
        <input type="text" name="first_name" value="${results[0].first_name}"/>
    </p>
    <p>
        <label>Фамилия:</label><br>
        <input type="text" name="last_name" value="${results[0].last_name}"/>
    </p>
	<p>
        <label>Email:</label><br>
        <input type="text" name="email" value="${results[0].email}"/>
    </p>
    <p>
        <label>Пол:</label><br>
        <input type="text" name="gender" value="${results[0].gender}"/>
    </p>
	<p>
        <button id="submitBtn" type="submit">Изменить</button>
    </p>
</form>


<h2>Дата регистрации: ${results[0].date}</h2>

<a href = "/">ВЫХОД</a>

	</body>
    </html>
`);

	});
});
app.post("/profile/:id", urlencodedParser, function(req, res) {

	const id = req.params.id;
	
	connection.execute(`UPDATE users SET 
						first_name = '${req.body.first_name}',
						last_name = '${req.body.last_name}',
						email = '${req.body.email}', 
						password = '${req.body.passwd}',
						gender = '${req.body.gender}'
						WHERE id = ${id} ;`,
		
		function(err, results, fields) {
		console.log(err);
		console.log(results); // собственно данные
	});

	res.redirect(`/profile/${id}`);

});
app.post('/upload/:id', function(req, res) {

	const id = req.params.id;
	req.files.photo.mv('public/pics/'+req.files.photo.name);

	connection.execute(`UPDATE users SET 
						photo = '${req.files.photo.name}'
						WHERE id = ${id} ;`,
		
		function(err, results, fields) {
		console.log(err);
		console.log(results); // собственно данные
	});

	res.redirect(`/profile/${id}`);
});


app.listen(3000, function(){
    console.log("Сервер запущен на http://localhost:3000/");
});
